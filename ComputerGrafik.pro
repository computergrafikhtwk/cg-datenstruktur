#-------------------------------------------------
#
# Project created by QtCreator 2016-06-20T23:29:19
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ComputerGrafik
TEMPLATE = app

SOURCES += main.cpp \
    3dObject/edge.cpp \
    3dObject/vertex.cpp \
    3dObject/face.cpp \
    dcelStruct/dcel.cpp \
    parser.cpp \
    geometryengine.cpp \
    mainwindow.cpp \
    openglwidget.cpp

HEADERS += \
    3dObject/edge.h \
    3dObject/vertex.h \
    3dObject/face.h \
    dcelStruct/dcel.h \
    dcelStruct/vertexposition.h \
    parser.h \
    geometryengine.h \
    mainwindow.h \
    openglwidget.h \
    targetver.h

FORMS    += mainwindow.ui

DISTFILES += \
    shader/fragmentshader.fsh \
    shader/vertexshader.vsh \
    cube.ply

RESOURCES += \
    resources.qrc
