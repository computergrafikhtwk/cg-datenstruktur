﻿# README #

Project Setup Guide:

1. Qt Download: [https://www.qt.io/download-open-source/](Link URL)
1. Anmelden und Qt Creator starten
1. In Qt:
    1. Extras > Einstellungen*
    1. Versionskontrolle > Git*
    1. Repository Browser setzen (git.exe) ![Screenshot_51.png](https://bitbucket.org/repo/rkEaEn/images/2089230300-Screenshot_51.png)
1. Git HTTP link kopieren: ![Screenshot_50.png](https://bitbucket.org/repo/rkEaEn/images/4093728547-Screenshot_50.png)
1. In Qt:
    1. Datei > Neu*
    1. Projekte > Projekt importieren > Git Clone*
    1. In Repository den kopierten Link eingeben
    1. Bestätigen
1. Have fun

1. https://discord.gg/010ReHpfhxYTD251h