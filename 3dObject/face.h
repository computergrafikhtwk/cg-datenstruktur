#ifndef FACE_H
#define FACE_H

#include "edge.h"
#include <cstddef>

class Edge;

class Face {
public:
    Face();

    Edge *edge = NULL;
};

#endif // FACE_H
