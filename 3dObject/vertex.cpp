#include "vertex.h"


//those are the methods declared in the class "punkt.h"

Vertex::Vertex(double x, double y, double z) {
    xCoord = x;
    yCoord = y;
    zCoord = z;
}

double Vertex::getX() const{
    return xCoord;
}

double Vertex::getY() const{
    return yCoord;
}

double Vertex::getZ() const{
    return zCoord;
}

bool Vertex::equals(Vertex* arg0){
    bool result = false;
    if(arg0 != NULL && xCoord == arg0->getX() && yCoord == arg0->getY() && zCoord == arg0->getZ()){
        result = true;
    }
    return result;
}
