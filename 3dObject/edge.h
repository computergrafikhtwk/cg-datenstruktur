#ifndef EDGE_H
#define EDGE_H
#include <cstddef>

#include "vertex.h"
#include "face.h"

class Vertex;
class Face;

class Edge {
public:
    Edge();

    Vertex *source;

    Edge *twin = NULL;
    Edge *next = NULL;
    Edge *prev = NULL;

    Face *face = NULL;
};

#endif // EDGE_H
