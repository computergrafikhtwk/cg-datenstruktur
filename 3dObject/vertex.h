#ifndef VERTEX_H
#define VERTEX_H

#include "edge.h"
#include <cstddef>

//example for a class in C++
//see "punkt.cpp" for the use of public methods

class Edge;

class Vertex {
public:
    double getX() const;
    double getY() const;
    double getZ() const;
    bool equals(Vertex* arg0);

    Edge *edge = NULL;

    Vertex(double x, double y, double z);

private:
    double xCoord;
    double yCoord;
    double zCoord;
};

#endif // VERTEX_H
