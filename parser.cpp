#include <iostream>
#include <string>

#include <fstream>
#include <locale>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <cstring>
#include <cstddef>

#include "parser.h"
#include "dcelStruct/dcel.h"
#include "3dObject/edge.h"
#include "3dObject/face.h"
#include "3dObject/vertex.h"

class Vertex;
class Edge;
class Face;
class Dcel;

using namespace std;

std::string extract_floats(std::ctype_base::mask category, std::string str, std::ctype<char> const& facet)
{
    using std::strlen;

    char const *begin = &str.front(),
        *end = &str.back();

    auto res = facet.scan_is(category, begin, end);

    begin = &res[0];
    end = &res[strlen(res)];

    return std::string(begin, end);
}

std::string extract_floats(std::string str)
{
    return extract_floats(std::ctype_base::digit, str,
        std::use_facet<std::ctype<char>>(std::locale("")));
}


// http://www.namespace-cpp.de/std/doku.php/howto/text_by_line


Edge* findTwin(Dcel arg0, Edge* arg1){
    Edge* result = NULL;
    if(arg1 != NULL){
        int count = arg0.getEdgeCount();
        Edge *e;
        bool sameSource = false;
        for (int var = 0; var < count; ++var) {
            e = arg0.getEdgeAt(var);
            if(e->source != NULL && arg1->source != NULL){
                sameSource =  e->source->equals(arg1->source);
                if(sameSource){
                    result = e->prev;
                    break;
               }
            }
        }
    }
    return result;
}


Dcel parse() {
    int temp = 0;
    Dcel dcel;

    std::string filename = "F:/bun.ply";
    //std::cout << "Dateinamen eingeben: ";
    //std::cin >> filename;

    int statusInFileReading = 0;
    int coordinatesRows;
    int i;

    std::string line, tempNumberString;
    std::string searchTerm("vertex");
    std::ifstream input(filename);

    if (!input){
        std::cerr << "Fehler beim Oeffnen der Datei " << filename << "\n";
        return dcel;
    }

    while (std::getline(input, line)){
        std::size_t found = line.find(searchTerm);
        std::stringstream ss(extract_floats(line));

        switch (statusInFileReading){
        case 0:

            std::cout << line << "\n";
            if (found != std::string::npos)
            {
                std::cout << "first 'vertex' found at: " << found << '\n';
                std::cout << "\n" << line << "\n";


                for (i = 0; i < line.length(); i++){
                    if (isdigit(line[i])) tempNumberString += line[i];
                }

                coordinatesRows = std::stoi(tempNumberString);

                std::cout << line << '\n'  << tempNumberString <<  "\n" << coordinatesRows << "\n";


                searchTerm = "end_header";
                statusInFileReading += 10;
            }

            break;
        case 10:
            if (found != std::string::npos)
            {
                std::cout << "Ende vom Header erreicht" << found << '\n';
                std::cout << "\n" << line << "\n";
                statusInFileReading += 10;
            }
            break;
        case 20:
            if (coordinatesRows > 0){

                std::string prefix = "-";
                double x , y , z;
                ss >> x >> y >> z;
                if (line.substr(0, prefix.size()) == prefix && x > 0) {
                    x = x*-1;
                }
                Vertex * vertex = new Vertex(x,y,z);
                std::cout << line <<  " X: " << vertex->getX() << " Y: " << vertex->getY() << " Z: " << vertex->getZ() << "\n";

                dcel.addVertex(vertex);

            //	addCoordinates(float coordinate.x, float coordinate.y, float coordinate.z, float confidence, float intensity);
                coordinatesRows--;
            }else{
                std::cout << "Face:\n";
                Face *face = new Face();
                dcel.addFace(face);
                //Face * faceP = dcel.getFaceAt(dcel.getFaceCount()-1);

                Edge* prev = NULL;
                int prevEdgeIndex = 0;
                int firstEdgeIndex;
                Edge* twin;
                Edge *edge;
                Vertex *v;
                int amount, index;
                ss >> amount;
                std::cout << "amount of vertices:" << amount << "\n";
                //TODO @Lennart prüfung ob links oder rechts
                for(int i = 0; i < amount; i++){
                    ss >> index;
                    edge = new Edge();
                    v = dcel.getVertexAt(index);
                    edge->face = face;
                    edge->source = v;
                    dcel.addEdge(edge);
                    //edgeP = dcel.getEdgeAt(dcel.getEdgeCount()-1);

                    twin = findTwin(dcel,edge);
                    if(twin != NULL){
                        edge->twin = twin;
                        twin->twin = edge;
                    }
                    //if (dcel.getEdgeCount()-1 != prevEdgeIndex) {
                    if(prev != NULL){
                        //prev = dcel.getEdgeAt(prevEdgeIndex);
                        prev->next = edge;
                        edge->prev = prev;
                    }
                    if( i == 0){
                        firstEdgeIndex = dcel.getEdgeCount()-1;
                    }
                    //prevEdgeIndex = dcel.getEdgeCount()-1;
                    prev = edge;
                }
                Edge * first = dcel.getEdgeAt(firstEdgeIndex);
                edge->next = first;
                first->prev = edge;

                face->edge = edge;

                for(int k = 0; k < amount; k++){
                                    //if (edge.source->getX() == -1 && edge.source->getY() == -1 && edge.source->getZ() == -1) {
                                    //    std::cout << "Punkt #0 existiert! \n";
                                    //}
                                    //if (edge.source->getX() == 1 && edge.source->getY() == -1 && edge.source->getZ() == -1) {
                                    //    std::cout << "Punkt #1 existiert! \n";
                                        //Edge twinTestEdge = edge;
                                        //Edge* twintest = twinTestEdge.twin;
                                        //std::cout << "Endpunkt 1: " << twinTestEdge.next->source->getX() << "\n";
                                        //std::cout << "Anfangspunkt 2:" << twintest->source->getX() << "\n";
                                        //if (twinTestEdge.next->source == twintest->source) {
                                           // std::cout <<"Yaaaaay \n";
                                        //}
                                    //}
                                    //if (edge.source->getX() == 1 && edge.source->getY() == 1 && edge.source->getZ() == -1) {
                                    //    std::cout << "Punkt #2 existiert! \n";
                                    //}
                                    //if (edge.source->getX() == -1 && edge.source->getY() == 1 && edge.source->getZ() == -1) {
                                    //    std::cout << "Punkt #3 existiert! \n";
                                    //}
                                    //if (edge.source->getX() == -1 && edge.source->getY() == -1 && edge.source->getZ() == 1) {
                                    //    std::cout << "Punkt #4 existiert! \n";
                                    //}
                                    //if (edge.source->getX() == 1 && edge.source->getY() == -1 && edge.source->getZ() == 1) {
                                     //   std::cout << "Punkt #5 existiert! \n";
                                    //}
                                    //if (edge.source->getX() == 1 && edge.source->getY() == 1 && edge.source->getZ() == 1) {
                                    //    std::cout << "Punkt #6 existiert! \n";
                                    //}
                                    //if (edge.source->getX() == -1 && edge.source->getY() == 1 && edge.source->getZ() == 1) {
                                    //    std::cout << "Punkt #7 existiert! \n";
                                    //}
                                    //std::cout << "Kante von X:" << edge.source->getX() << " Y: " << edge.source->getY() << " Z: " << edge.source->getZ() << "\n";
                                    //std::cout << "Twin von " << twinTest->source->getX() << twinTest->source->getY() << twinTest->source->getZ() << "\n";
                                }
                //TODO
                /*
                ss >> face.number1 >> face.number2;
                if (face.number2 < 0) face.number2 = NULL;

                std::cout << line << " Wert 1: " << face.number1 << " Wert 2: " << face.number2 << "\n";
                */
                // addFaces(int Number1, int Number2);
            }

                // statusInFileReading += 10;
            break;
        case 40:

            break;
        default:
            std::cerr << "Fehler beim Verarbeiten der Datei " << filename << "\n Ist File womoeglich korrupt.";
            return dcel;
            break;

        }
    }

    return dcel;
}
