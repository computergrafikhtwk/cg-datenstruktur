#ifndef DCEL_H
#define DCEL_H
#include <vector>
#include <3dObject/vertex.h>
#include <3dObject/edge.h>
#include <3dObject/face.h>


class Dcel
{
public:
    Dcel();
    void addEdge(Edge *arg0);
    void addFace(Face *arg0);
    void addVertex(Vertex *arg0);
    Vertex * getVertexAt(int arg0);
    Edge *getEdgeAt(int arg0);
    Face *getFaceAt(int arg0);
    int getEdgeCount();
    int getFaceCount();
private:
    std::vector<Vertex *> vertices;
    std::vector<Edge *> edges;
    std::vector<Face *> faces;
};

#endif // DCEL_H
