#include "dcel.h"

Dcel::Dcel() {

}

void Dcel::addEdge(Edge *arg0) {
    this->edges.push_back(arg0);
}

void Dcel::addFace(Face *arg0) {
    this->faces.push_back(arg0);
}

void Dcel::addVertex(Vertex * arg0){
    this->vertices.push_back(arg0);
}

Vertex* Dcel::getVertexAt(int arg0){
    return this->vertices.at(arg0);
}

Edge *Dcel::getEdgeAt(int arg0){
     return (this->edges.at(arg0));
}

Face *Dcel::getFaceAt(int arg0){
     return this->faces.at(arg0);
}

int Dcel::getEdgeCount(){
    return this->edges.size();
}

int Dcel::getFaceCount(){
    return this->faces.size();
}

