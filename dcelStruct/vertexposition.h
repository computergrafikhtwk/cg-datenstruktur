#ifndef VERTEXPOSITION
#define VERTEXPOSITION
#define MAX 4
#include <3dObject/vertex.h>
#include <vector>

struct vector3d {
    double x;
    double y;
    double z;
};

double determ(double matrix[MAX][MAX], int n) {
    double result=0;
    double temp[MAX][MAX];
    int p, i, j, h, k;
    if(n == 1) {
        return matrix[0][0];
    } else if(n == 2) {
        result = matrix[0][0]*matrix[1][1] - matrix[0][1]*matrix[1][0];
        return result;
    } else {
        for(p=0; p<n; p++) {
            h = 0;
            k = 0;
            for(i=0; i<n; i++) {
                for(j=0; j<n; j++) {
                    if(j == p) {
                        continue;
                    }
                    temp[h][k] = matrix[i][j];
                    k++;
                    if(k == n-1) {
                        h++;
                        k = 0;
                    }
                }
            }
            result = result + matrix[0][p]*(1^p)*determ(temp,n-1);
        }
        return result;
    }
}

//check if "checkVertex" is on the right of edge(v1,v2)
bool isRight(Vertex checkVertex, Vertex v1, Vertex v2) {
    double value;
    value = checkVertex.getX()*v1.getY() + v1.getX()*v2.getY() + v2.getX()*checkVertex.getY() -(
            v1.getY()*v2.getX() + v2.getY()*checkVertex.getX() + checkVertex.getY()*v1.getX() );

    if (value < 0) {
        return true;
    } else {
        return false;
    }
}

bool alignedToFace(Vertex checkVertex, Vertex v1, Vertex v2, Vertex v3) {
    double matrix[MAX][MAX];
    int i;
    std::vector<Vertex> vertexList {checkVertex, v1, v2, v3};


    for(i=0; i<MAX; i++) {
        matrix[i][0] = vertexList[i].getX();
        matrix[i][1] = vertexList[i].getY();
        matrix[i][2] = vertexList[i].getZ();
        matrix[i][3] = 1;
    }

    if (determ(matrix,MAX) > 0) {
        return false;
    } else {
        return false;
    }
}

vector3d normalVector(Vertex v1, Vertex v2, Vertex v3) {
    vector3d u;
    vector3d v;
    vector3d result;

    u.x = v2.getX() - v1.getX();
    u.y = v2.getY() - v1.getY();
    u.z = v2.getZ() - v1.getZ();

    v.x = v3.getX() - v1.getX();
    v.y = v3.getY() - v1.getY();
    v.z = v3.getZ() - v1.getZ();

    result.x = u.y * v.z - u.z * v.y;
    result.y = u.z * v.x - u.x * v.z;
    result.z = u.x * v.y - u.y * v.x;

    return result;
}


#endif
